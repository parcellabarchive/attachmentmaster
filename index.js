var settings = {
  path: {
    destinationDir: '/home/emailusers/',
    watchedDir: '../email/raw/',
    archiveDir: '../email/raw/archive/'
  }
};

// var settings = {
//   path: {
//     destinationDir: './out/',
//     watchedDir: './test/watch/',
//     archiveDir: './test/archive/'
//   }
// };

// utils

var _ = require('underscore');

// file system

var Watcher = require('chokidar');
var fs = require('fs');

// control flow

var Async = require('async');

// parsing

var DecoderIconv = require('iconv-lite');
var DecoderMIME = require('mimelib');
var MailParser = require("mailparser").MailParser;

/////////////
// Runtime //
/////////////

Watcher.watch(settings.path.watchedDir, {
    persistent: true,
    ignored: /[\/\\]\./,
    ignoreInitial: true,
    followSymlinks: false,
    depth: 0,
    awaitWriteFinish: {
      stabilityThreshold: 5000,
      pollInterval: 100
    }
  }).on('add', function(path, event) {

    console.log(new Date().toISOString() + ' > New file: ' + path);

    Async.waterfall([
      function readRawFile(callback) {

        fs.readFile(path, function(err, data) {
          if (err) console.log(err);

          callback(null, data);

        });

      },
      function parseMime(mime, callback) {

        var mp = new MailParser({
          defaultCharset: 'utf8'
        });

        mp.on('end', function(email) {
          callback(null, email.attachments);
        });

        mp.write(mime);
        mp.end();

      },
      function getCsvFromAttachments(attachments, callback) {

        var csvAttachment = null;
        for (var i = 0; i < attachments.length; i++) {
          if (attachments[i].generatedFileName.indexOf('.csv') > -1 || attachments[i].generatedFileName.indexOf('.txt') > -1) {
            csvAttachment = attachments[i];
          }
        }

        callback(null, csvAttachment);

      },
      function parseAttachment(csvAttachment, callback) {
        if (_.isNull(csvAttachment)) callback(new Error('No CSV attachment'));

        if (csvAttachment.transferEncoding == 'base64') {

          callback(null, new Buffer(DecoderIconv.decode(csvAttachment.content, csvAttachment.transferEncoding), 'base64').toString('utf8'), csvAttachment.generatedFileName);

        } else if (csvAttachment.transferEncoding == 'quoted-printable') {

          callback(null, DecoderMIME.decodeQuotedPrintable(csvAttachment.content), csvAttachment.generatedFileName);

        } else if (DecoderIconv.encodingExists(csvAttachment.transferEncoding)) {

          callback(null, DecoderIconv.decode(csvAttachment.content, csvAttachment.transferEncoding), csvAttachment.generatedFileName);

        } else callback(new Error('Encoding >' + csvAttachment.transferEncoding + '< not found'));

      },
      function saveToFile(contents, filename, callback) {

        var destPath = settings.path.destinationDir;
        destPath += filenameFromPath(path).split('_')[0] + '/';
        destPath += Math.floor(new Date() / 1000) + '_';
        destPath += filename;

        console.log(new Date().toISOString() + ' > storing --> ' + destPath);

        fs.writeFile(destPath, contents, 'utf8', function(err) {
          if (err) callback(err);
          else callback(null);
        });

      }
    ], function(err, result) {
      if (err) console.log(err);

      console.log(new Date().toISOString() + ' > ' + path + ' --> ' + settings.path.archiveDir + filenameFromPath(path));
      fs.rename(path, settings.path.archiveDir + filenameFromPath(path), function(err) {
        if (err) console.log(err);
      });

    });

  })
  .on('error', function(error) {
    console.log(error);
  });

/**
 * returns the last element of a file path, i.e. the file's name
 * @param  {String} path path to file
 * @return {String}      name of file
 */
function filenameFromPath(path) {
  var elements = path.split('/');
  return elements[elements.length - 1];
}
